﻿using System.Collections.Generic;

namespace Dsl.DynamicProvider.Domain.DynamicProvider
{

    public class Operation
    {
        public string Name { get; set; }
    }

    public class DynamicProvider : IType, IRequire, IOperation
    {
        private readonly IContext _context;

        public string Event = null;
        public string Type = null;

        public List<IService> Services = new List<IService>();
        public List<OperationDelegate> Operations { get; set; } = new List<OperationDelegate>();

        private static List<DynamicProvider> definiciones = null;

        public DynamicProvider(IContext context)
        {
            _context = context;
        }

        public IType OnEvent(string eventName)
        {
            if (definiciones == null) definiciones = new List<DynamicProvider>();
            var dp = new DynamicProvider(eventName);
            definiciones.Add(dp);
            return dp;
        }

        private DynamicProvider(string @event)
        {
            Event = @event;
        }

        public IRequire WhenType(string type)
        {
            Type = type;
            return this;
        }
        
        public IRequire Require(string serviceName)
        {
            Services.Add(new Service(serviceName));
            return this;
        }

        public IOperation DefineOperation(OperationDelegate operation)
        {
            Operations.Add(operation);
            operation.Invoke(_context, Services.ToArray());
            return this;
        }
    }
}
