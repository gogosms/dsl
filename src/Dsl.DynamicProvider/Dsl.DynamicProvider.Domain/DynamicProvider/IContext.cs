﻿namespace Dsl.DynamicProvider.Domain.DynamicProvider
{
    public interface IContext
    {
        string Id { get; }
    }

    public class Context : IContext
    {
        public Context(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }
}