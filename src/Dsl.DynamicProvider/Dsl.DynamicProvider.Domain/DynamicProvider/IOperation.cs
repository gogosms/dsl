﻿namespace Dsl.DynamicProvider.Domain.DynamicProvider
{
    public interface IOperation
    {
        IOperation DefineOperation(OperationDelegate action);

    }
}