﻿namespace Dsl.DynamicProvider.Domain.DynamicProvider
{
    public interface IRequire : IOperation
    {
        IRequire Require(string serviceName);
    }
}