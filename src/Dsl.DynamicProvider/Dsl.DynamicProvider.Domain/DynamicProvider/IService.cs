﻿namespace Dsl.DynamicProvider.Domain.DynamicProvider
{
    public interface IService
    {
    }

    public class Service : IService
    {
        private readonly string _name;

        public Service(string name)
        {
            _name = name;
        }
        
    }
}