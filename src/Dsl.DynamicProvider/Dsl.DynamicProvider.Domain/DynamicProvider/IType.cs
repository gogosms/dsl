﻿namespace Dsl.DynamicProvider.Domain.DynamicProvider
{
    public interface IType
    {
        IRequire WhenType(string type);
    }
}