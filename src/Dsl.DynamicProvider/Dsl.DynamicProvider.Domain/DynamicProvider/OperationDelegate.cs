﻿namespace Dsl.DynamicProvider.Domain.DynamicProvider
{
    public delegate Operation OperationDelegate(IContext context, IService[] services);
}