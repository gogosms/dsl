﻿namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder
{
    public class SoldierGenericAgeBuilder<T> : 
        SoldierGenericInfoBuilder<SoldierGenericAgeBuilder<T>> where T :
        SoldierGenericAgeBuilder<T>
    {
        public T WithAge(int age)
        {
            Soldier.Age = age;
            return (T)this;
        }
    }
}