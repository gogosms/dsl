﻿using Dsl.DynamicProvider.Domain.HeroDsl;

namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder
{
    public abstract class SoldierGenericBuilder
    {
        protected Soldier Soldier;

        protected SoldierGenericBuilder()
        {
            Soldier = new Soldier();
        }

        public Soldier Build() => Soldier;
    }
}