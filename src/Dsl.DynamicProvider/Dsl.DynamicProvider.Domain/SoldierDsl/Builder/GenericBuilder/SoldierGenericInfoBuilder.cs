﻿namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder
{
    public class SoldierGenericInfoBuilder<T> : 
        SoldierGenericBuilder where T : 
        SoldierGenericInfoBuilder<T>
    {
        public T SetName(string name)
        {
            Soldier.Name = name;
            return (T)this;
        }
    }
}