﻿using Dsl.DynamicProvider.Domain.HeroDsl;

namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder
{
    public class SoldierGenericWeaponsBuilder<T> :
        SoldierGenericAgeBuilder<SoldierGenericWeaponsBuilder<T>> where T : 
        SoldierGenericWeaponsBuilder<T>
    {
        public T ToEquip(IWeapon weapon)
        {
            Soldier.Weapons.Add(weapon);
            return (T)this;
        }
    }
}