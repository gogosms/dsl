﻿using Dsl.DynamicProvider.Domain.HeroDsl;

namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder
{
    public class SoldierBuilder: ISoldierBuilderAge, ISoldierBuilderWeapons
    {
        private Soldier _soldier;

        public ISoldierBuilderAge Create(string name)
        {
            _soldier = new Soldier (name);
            return this;
        }

        public ISoldierBuilderWeapons WithAge(int age)
        {
            _soldier.Age = age;
            return this;
        }

        public ISoldierBuilderWeapons ToEquip(IWeapon weapon)
        {
            _soldier.Weapons.Add(weapon);
            return this;
        }

        public Soldier Value()
        {
            return _soldier;
        }
    }

    public interface ISoldierBuilderAge
    {
        ISoldierBuilderWeapons WithAge(int age);
    }

    public interface ISoldierBuilderWeapons
    {
        ISoldierBuilderWeapons ToEquip(IWeapon weapon);
    }
}