﻿using Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder;

namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.SpecificBuilders
{
    public class BrigadierGeneralBuilder : SoldierGenericWeaponsBuilder<BrigadierGeneralBuilder>
    {
        public static BrigadierGeneralBuilder NewBoss => new BrigadierGeneralBuilder();
    }
}