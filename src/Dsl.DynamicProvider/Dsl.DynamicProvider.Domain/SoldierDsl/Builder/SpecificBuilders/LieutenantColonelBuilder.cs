﻿using Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder;

namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.SpecificBuilders
{
    public class LieutenantColonelBuilder : SoldierGenericWeaponsBuilder<LieutenantColonelBuilder>
    {
        public static LieutenantColonelBuilder NewLieutenantColonel => new LieutenantColonelBuilder();
    }
}