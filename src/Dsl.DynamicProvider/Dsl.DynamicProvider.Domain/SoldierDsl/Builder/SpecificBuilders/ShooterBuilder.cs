﻿using Dsl.DynamicProvider.Domain.SoldierDsl.Builder.GenericBuilder;

namespace Dsl.DynamicProvider.Domain.SoldierDsl.Builder.SpecificBuilders
{
    public class ShooterBuilder : SoldierGenericWeaponsBuilder<ShooterBuilder>
    {
        public static ShooterBuilder NewShooter => new ShooterBuilder();
    }
}
