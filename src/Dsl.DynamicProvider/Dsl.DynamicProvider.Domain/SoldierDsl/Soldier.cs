﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dsl.DynamicProvider.Domain.HeroDsl
{
    public class Soldier
    {
        public Soldier()
        {
        }
        public Soldier(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public int Age { get; set; }
        public List<IWeapon> Weapons { get; set; } = new List<IWeapon>();
    }
}
