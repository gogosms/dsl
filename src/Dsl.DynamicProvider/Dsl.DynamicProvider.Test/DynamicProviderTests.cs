using System.Collections.Generic;
using Dsl.DynamicProvider.Domain.DynamicProvider;
using NUnit.Framework;

namespace Dsl.DynamicProvider.Test
{
    public class DynamicProviderTests
    {
        private Domain.DynamicProvider.DynamicProvider _dynamicProvider;

        [SetUp]
        public void Setup()
        {
            _dynamicProvider = new Domain.DynamicProvider.DynamicProvider(new Context("Context1"));
        }

        [Test]
        public void Must_()
        {
            _dynamicProvider.DefineOperation((context, services) => new Operation());
            Operation operation01 = null;
            var expectedServices = new List<IService>();
            var expectedContext = new Context("Test01");
            _dynamicProvider
                .OnEvent("AgregateResource")
                .WhenType("Resource")
                    .Require("IServiceA")
                    .Require("IServiceB")
                    .Require("IServiceC")
                .DefineOperation((context, services) =>
                {
                    expectedServices.AddRange(services);
                    return operation01 = new Operation
                    {
                        Name = "Operation01"
                    };
                });

            Assert.NotNull(operation01);
            Assert.That(expectedServices.Count, Is.EqualTo(3));

        }
    }
}