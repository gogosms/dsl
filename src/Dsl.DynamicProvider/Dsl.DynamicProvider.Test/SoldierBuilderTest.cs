using Dsl.DynamicProvider.Domain.HeroDsl;
using Dsl.DynamicProvider.Domain.SoldierDsl.Builder;
using Dsl.DynamicProvider.Domain.SoldierDsl.Builder.SpecificBuilders;
using NUnit.Framework;

namespace Dsl.DynamicProvider.Test
{
    public class SoldierBuilderTest
    {
        [Test]
        public void Must_Create_Soldier_With_Fluent_Interface()
        {
            var soldierBuilder = new SoldierBuilder();
            var soldierName = "rvaldez";
            var age = 34;

            soldierBuilder
                .Create(soldierName)
                    .WithAge(age)
                    .ToEquip(new Gun())
                    .ToEquip(new MachineGun());

            var hero = soldierBuilder.Value();
            Assert.IsNotNull(hero);
            Assert.That(hero.Name, Is.EqualTo(soldierName));
            Assert.That(hero.Age, Is.EqualTo(age));
            Assert.That(hero.Weapons.Count, Is.EqualTo(2));
        }

        [Test]
        public void Must_Create_Specific_Soldier()
        {
            
            var soldierName = "rvaldez";
            var age = 34;
            var brigadier = BrigadierGeneralBuilder
                .NewBoss
                .SetName(soldierName)
                .WithAge(34)
                .ToEquip(new Gun())
                .ToEquip(new MachineGun())
                .Build();
            
            Assert.IsNotNull(brigadier);
            Assert.That(brigadier.Name, Is.EqualTo(soldierName));
            Assert.That(brigadier.Age, Is.EqualTo(age));
            Assert.That(brigadier.Weapons.Count, Is.EqualTo(2));
        }

    }
}